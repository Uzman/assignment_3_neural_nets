import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by oguz on 26/10/15.
 */
public class Main{
    private static final int inputCount = 7;
    private static Neuron[] firstLayer;
    private static Neuron[] outputLayer;

    private static  int inputLayerElementCount ;
    {
        Random r = new Random();
        inputLayerElementCount = r.nextInt(99)+1;
    }

    public static void main(String[] args) {






        String txt = "QDH FAX YN XKT ANRKDHXTJ PDROMDXTHG FQ XKT ANQDGKYFNDPET  TNJ  FQ\n" +
                "XKT  MTGXTHN  GWYHDE  DHU  FQ  XKT CDEDSI EYTG D GUDEE ANHTCDHJTJ\n" +
                "ITEEFM GAN.\n" +
                "\n" +
                "FHPYXYNC XKYG DX D JYGXDNRT FQ HFACKEI NYNTXI-XMF  UYEEYFN  UYETG\n" +
                "YG  DN  AXXTHEI YNGYCNYQYRDNX EYXXET PEAT CHTTN WEDNTX MKFGT DWT-\n" +
                "JTGRTNJTJ EYQT QFHUG DHT GF DUDBYNCEI WHYUYXYVT XKDX  XKTI  GXYEE\n" +
                "XKYNO JYCYXDE MDXRKTG DHT D WHTXXI NTDX YJTD.\n" +
                "\n" +
                "XKYG WEDNTX KDG - FH HDXKTH KDJ - D WHFPETU, MKYRK MDG XKYG: UFGX\n" +
                "FQ  XKT  WTFWET  FN  YX MTHT ANKDWWI QFH WHTXXI UARK FQ XKT XYUT.\n" +
                "UDNI GFEAXYFNG MTHT GACCTGXTJ QFH XKYG WHFPETU, PAX UFGX FQ XKTGT\n" +
                "MTHT  EDHCTEI  RFNRTHNTJ MYXK XKT UFVTUTNXG FQ GUDEE CHTTN WYTRTG\n" +
                "FQ WDWTH, MKYRK YG FJJ PTRDAGT FN XKT MKFET YX MDGN'X  XKT  GUDEE\n" +
                "CHTTN WYTRTG FQ WDWTH XKDX MTHT ANKDWWI.\n" +
                "\n" +
                "DNJ GF XKT WHFPETU HTUDYNTJ; EFXG FQ XKT WTFWET  MTHT  UTDN,  DNJ\n" +
                "UFGX FQ XKTU MTHT UYGTHDPET, TVTN XKT FNTG MYXK JYCYXDE MDXRKTG.\n" +
                "\n" +
                "UDNI MTHT YNRHTDGYNCEI FQ XKT FWYNYFN XKDX XKTI'J DEE UDJT D  PYC\n" +
                "UYGXDOT  YN  RFUYNC  JFMN  QHFU XKT XHTTG YN XKT QYHGX WEDRT. DNJ\n" +
                "GFUT GDYJ XKDX TVTN XKT XHTTG KDJ PTTN D PDJ UFVT,  DNJ  XKDX  NF\n" +
                "FNT GKFAEJ TVTH KDVT ETQX XKT FRTDNG.\n" +
                "\n" +
                "DNJ XKTN, FNT XKAHGJDI, NTDHEI XMF XKFAGDNJ ITDHG DQXTH  FNT  UDN\n" +
                "KDJ  PTTN NDYETJ XF D XHTT QFH GDIYNC KFM CHTDX YX MFAEJ PT XF PT\n" +
                "NYRT XF WTFWET QFH D RKDNCT, FNT CYHE GYXXYNC FN  KTH  FMN  YN  D\n" +
                "GUDEE  RDQT  YN  HYROUDNGMFHXK GAJJTNEI HTDEYBTJ MKDX YX MDG XKDX\n" +
                "KDJ PTTN CFYNC MHFNC DEE XKYG XYUT, DNJ GKT QYNDEEI ONTM KFM  XKT\n" +
                "MFHEJ  RFAEJ  PT  UDJT  D  CFFJ DNJ KDWWI WEDRT. XKYG XYUT YX MDG\n" +
                "HYCKX, YX MFAEJ MFHO, DNJ NF FNT MFAEJ  KDVT  XF  CTX  NDYETJ  XF\n" +
                "DNIXKYNC.\n" +
                "\n" +
                "GDJEI, KFMTVTH, PTQFHT GKT RFAEJ CTX XF D WKFNT  XF  XTEE  DNIFNT-\n" +
                "DPFAX  YX,  D  XTHHYPEI GXAWYJ RDXDGXHFWKT FRRAHHTJ, DNJ XKT YJTD\n" +
                "MDG EFGX QFHTVTH.";


        HashMap<String, String > map = new HashMap<>();

        map.put("F","o");
        map.put("Q","f");
        map.put("D","a");
        map.put("X","t");
        map.put("T","e");
        map.put("K","h");
        map.put("N","n");
        map.put("V","v");
        map.put("H","r");
        map.put("I","y");
        map.put("J","d");
        map.put("Y","i");
        map.put("G","s");
        map.put("M","w");
        map.put("A","u");
        map.put("E","l");
        map.put("R","c");
        map.put("O","k");
        map.put("C","g");
        map.put("P","b");
        map.put("W","p");
        map.put("S","x");
        map.put("U","m");
        map.put("B","z");

        List<String> sorted = new ArrayList<>(map.keySet());
        String keysText = "";
        Collections.sort(sorted);
        for(String key: sorted){
            txt = txt.replaceAll(key, map.get(key));
            keysText += key + " --> "+ map.get(key) + "\n";
        }

/*
        txt  = txt
                .replaceAll("F", "o")
                .replaceAll("Q", "f")
                .replaceAll("D", "a")
                .replaceAll("X", "t")
                .replaceAll("T", "e")
                .replaceAll("K", "h")
                .replaceAll("N", "n")
                .replaceAll("V","v")
                .replaceAll("H", "r")
                .replaceAll("I", "y")
                .replaceAll("J", "d")
                .replaceAll("Y", "i")
                .replaceAll("G", "s")
                .replaceAll("M", "w")
                .replaceAll("A", "u")
                .replaceAll("E", "l")
                .replaceAll("R", "c")
                .replaceAll("O", "k")
                .replaceAll("C", "g")
                .replaceAll("P", "b")
                .replaceAll("W", "p")
                .replaceAll("S", "x")
                .replaceAll("U", "m")
                .replaceAll("B", "z")
        ;
        */

        try {
            PrintWriter out = new PrintWriter("filename.txt");
            out.write(txt);
            out.close();
            out = new PrintWriter("keylist.txt");
            out.write(keysText);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.printf(txt);

return;

/*


        //start initialization
        Scanner scanner = new Scanner(System.in);

        int firstLayerCount, outputLayerCount;

        Random rn = new Random();
        firstLayerCount = 4;//Number of neurons in input layer
        outputLayerCount = 4;//Number of neurons in output layer


        firstLayer = new Neuron[firstLayerCount];
        outputLayer = new Neuron[outputLayerCount];

        int m = inputCount;
        for(int i = 0; i < firstLayerCount; i++){
            firstLayer[i] = new Neuron(inputCount,false);//create input layer neurons
        }

        for (int i = 0; i < outputLayerCount; i++){
            outputLayer[i] = new Neuron(firstLayerCount);//create input layer neurons
        }

        while (true){
            System.out.printf("Please enter %d digit binary input and press enter\n", inputCount);
            String strInput = scanner.nextLine();
            //Start to check if user input is valid
            if(strInput.length() != inputCount){
                System.out.printf("Invalid entry, must be %d digits long.\n", inputCount);
                continue;
            }
            boolean allBinary = true;
            for(int i = 0; i < inputCount; i++){
                if(strInput.charAt(i) != '1' && strInput.charAt(i) != '0'){
                    allBinary = false;
                    break;
                }
            }
            if(!allBinary){
                System.out.println("All digits must be binary\n");
                continue;
            }

            //User input is valid

            double[] inputs = new double[inputCount];//create input array
            double[] firstLayerOutput = new double[firstLayerCount];//create first layer's output values

            for(int i = 0; i < inputCount; i++){
                inputs[i] = strInput.charAt(i) == '1' ? 1 : 0;//convert chars to int
            }

            System.out.println("Input Layer results");
            for(int i= 0; i < firstLayerCount; i++){//traverse every neuron in first layer
                firstLayerOutput[i] = firstLayer[i].getOutputForInput(inputs);
                //get output for every neuron in first layer and save
            }
            System.out.println("\n\n\n\nEnd of input layer neuron outputs\nOutput Layer:");
            double[] secondLayerOutput = new double[outputLayerCount];//array to hold the output of the output layer

            for(int i= 0; i < outputLayerCount; i++){
                secondLayerOutput[i] = outputLayer[i].getOutputForInput(firstLayerOutput);
            }



        }
        */

    }

    public static class Neuron {

        public double biasWeight ;
        public double[] weights;
        public Neuron(int numberOfInputs){
            this(numberOfInputs, true);
        }
        public Neuron(int numberOfInputs, boolean assignWeights){
            if(assignWeights) {
                weights = createNWeights(numberOfInputs);
                biasWeight = createWeight();
            } else {
                weights = new double[numberOfInputs];
                biasWeight = 1;
            }
        }

        public int getOutputForInput(double[] inputs){

            System.out.printf("Bias weight: %f \nMy weights:",biasWeight);
            for(int i = 0; i< inputs.length;i++){
                System.out.print(weights[i]+ " ");//multiply bias and weight
            }
            System.out.print("\nMy input:");
            for(int i = 0; i< inputs.length;i++){
                System.out.print(inputs[i]+ " ");
            }
            System.out.println();
            if(inputs.length != weights.length)
                throw new RuntimeException("Input count should be equal to number of weights");
            double sum = biasWeight;//add bias to sum
            for(int i = 0; i < inputs.length; i++){
                sum += inputs[i]*weights[i];//multiply every input and corresponding weight and add to sum
            }
            System.out.printf("Sum: %f\n", sum);
            return sum > 0 ? 1 : 0;//using step function
        }
    }


    public static double createWeight(){//create weight between -0.5 and +0.5
        Random random = new Random();
        return random.nextDouble()-0.5;
    }

    public static double[] createNWeights(int n){//create weight array with selected count
        double[] ret = new double[n];
        for(int i = 0; i < n; i++){
            ret[i] = createWeight();
        }
        return ret;
    }

}
