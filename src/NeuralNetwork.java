import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;


/**
 * Created by oguz on 01/11/15.
 */
public class NeuralNetwork {

    public static int inputLayerNeuronCount , hiddenLayerNeuronCount = 2, outputLayerNeuronCount ;
    public static int trainCount = 3000;
    public static final double hiddenLayerLearningRate = 0.05, outputLayerLearningRate = 0.05;
    public static final long randomSeed = System.currentTimeMillis();//You can define any random seed.

    public static class Pattern{
        List<Double> input = new ArrayList<>();
        List<Double> output = new ArrayList<>();
    }

    public static void main(String[] args) {

        List<Pattern> patterns = new ArrayList<>();

        int patternCount = 0;
        try(BufferedReader br = new BufferedReader(new FileReader("training.dat"))) {
            br.readLine();
            String[] info = br.readLine().trim().split(" +");
            patternCount = Integer.parseInt(info[1].split("=")[1]);
            int n = Integer.parseInt(info[2].split("=")[1]);
            int m = Integer.parseInt(info[3].split("=")[1]);
            inputLayerNeuronCount = n;
            outputLayerNeuronCount = m;

            for(String line; (line = br.readLine()) != null; ) {
                line = line.trim();
                //System.out.println(line);
                String[] parts = line.split(" +");

                Pattern p = new Pattern();
                p.input.add(1.0);
                for(int i = 0; i < n; i++){
                    p.input.add(Double.parseDouble(parts[i]));
                }
                for(int i = n; i < n+m; i++){
                    p.output.add(Double.parseDouble(parts[i]));
                }
                patterns.add(p);

            }
            // line is not visible here.
        } catch (Exception e){
            e.printStackTrace();
            System.out.println("Error parsing the input file");
            return;
        }


        double[][] hiddenLayerWeights = new double[hiddenLayerNeuronCount][inputLayerNeuronCount + 1];//+1 is for BIAS
        double[][] outputLayerWeights = new double[outputLayerNeuronCount][hiddenLayerNeuronCount +1];//+1 is for BIAS

        Random random = new Random(randomSeed);

        for(int i = 0; i < hiddenLayerNeuronCount; i++){
            for (int j = 0; j <= inputLayerNeuronCount; j++){
                //hiddenLayerWeights[i][j] = random.nextDouble()-0.5;//assigned a weight between -0.5 and +0.5
                hiddenLayerWeights[i][j] = 0.5;
            }

        }

        for(int i = 0; i < outputLayerNeuronCount; i++){
            for (int j = 0; j <= hiddenLayerNeuronCount; j++){
                //outputLayerWeights[i][j] = random.nextDouble()-0.5;//assigned a weight between -0.5 and +0.5
                outputLayerWeights[i][j] = 0.5;
            }
        }

        Scanner scanner = new Scanner(System.in);

        for(int k = 0; k < patternCount; k++) {
            Pattern p = patterns.get(k);

            //User input is valid

            Double[] inputs = new Double[inputLayerNeuronCount+1];//create input array
            Double[] hiddenLayerOutputs = new Double[hiddenLayerNeuronCount+1];//+1 is for bias
            Double[] hiddenLayerWeightInputSum = new Double[hiddenLayerNeuronCount+1];//+1 is for bias
            Double[] outputLayerOutputs = new Double[outputLayerNeuronCount];//create array to save outputs
            Double[] outputLayerWeightInputSum = new Double[outputLayerNeuronCount];//create array to save outputs
            Double[] deltasForOutputLayer = new Double[outputLayerNeuronCount];
            inputs = p.input.toArray(inputs);

            hiddenLayerOutputs[0] = 1.0;
            for(int i = 0; i < hiddenLayerNeuronCount; i++) {
                double[] weights = hiddenLayerWeights[i];//get an hidden layer neuron's weights
                double totalSum = 0;
                for (int j = 0; j <= inputLayerNeuronCount; j++) {//multiply neurons weights with input vector and add it
                    totalSum += weights[j]*inputs[j];
                }

                //Print the result, use step function
                hiddenLayerOutputs[i+1] = tanh(totalSum);
                hiddenLayerWeightInputSum[i+1] = totalSum;
                //System.out.printf("Output %d: %f, step function: %d\n", i +1 , outputLayerOutputs[i], outputLayerOutputs[i]>0 ? 1: 0);
            }

            for(int i = 0; i < outputLayerNeuronCount; i++) {
                double[] weights = outputLayerWeights[i];//get an output neuron's weights
                double totalSum = 0;
                for (int j = 0; j <= hiddenLayerNeuronCount; j++) {//multiply neurons weights with input vector and add it
                    totalSum += weights[j]*hiddenLayerOutputs[j];
                }

                //Print the result, use step function
                outputLayerOutputs[i] = tanh(totalSum);
                outputLayerWeightInputSum[i] = totalSum;
                System.out.printf("Output %d: %f, tanh: %f\n", i +1 , totalSum, outputLayerOutputs[i]);
            }
        }

    }

    public static double tanh(double x){
        return Math.tanh(x);
    }
    
    public static double derivativeTanh(double x){
        return Math.pow(1.0 / Math.cosh(x),2);
    }


}
